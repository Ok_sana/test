
// 1. опредили зону события
// 2. получили дата атрибут таргета
// 3. получить контент
// 4. перебираем контент

// ccntent[i]  ===
//tabs.addEventListener("click", (event) => console.log(event.target.getAttribute("data-words")));

const tabs = document.getElementsByClassName("tabs")[0];
const content = document.getElementsByClassName("tabs-content") [0];

tabs.addEventListener("click", function f(event) {

    let tabAttr = event.target.getAttribute("data-words");
    for (let e = 0; e<tabs.children.length; e++){
             tabs.children[e].classList.remove("active")
            }
    event.target.classList.add("active");

    for(let i = 0; i < content.children.length; i++){
        if (content.children[i].className != "hidden") {
            content.children[i].classList.add("hidden")
        }
        else if (content.children[i].getAttribute('data-content') === tabAttr) {
            content.children[i].classList.remove("hidden")
        }
    }
})
